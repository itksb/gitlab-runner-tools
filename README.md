# gitlab-runner-tools - simple bash scripts to start gitlab runner quickly.

- install docker
- clone this project
- use ./start.sh to start gitlab-runner
- use ./stop.sh to stop gitlab runner
- use ./register to register gitlab-runner